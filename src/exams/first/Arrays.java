package exams.first;

import java.util.Scanner;

public class Arrays {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // create scanner
		String[] words = new String[5]; // create array
		for (int i=0; i < words.length; i++) { // fill array
			System.out.print("Enter in a word: ");
			words[i] = input.nextLine();
		}
			
		boolean same = false;
		for (int k=1; k<words.length; k++) { // check if index 0 equals any other index
			if (words[0].equals(words[k])) {
				same = true;
			}
		}
		
		boolean same2 = false;
		for (int k=2; k<words.length; k++) { // check if index 1 equals any other index
			if (words[1].equals(words[k])) {
				same2 = true;
			}
		}
		
		boolean same3 = false;
		for (int k=3; k<words.length; k++) { // check if index 2 equals any other index
			if (words[2].equals(words[k])) {
				same3 = true;
			}
		}
		
		boolean same4 = false;
		for (int k=4; k<words.length; k++) { // check if index 3 equals any other index
			if (words[3].equals(words[k])) {
				same4 = true;
			}								// don't need to check index 4
		}
		
		if (same == true) { 
			System.out.println(words[0]); // print index 0 if it was equal to any other index
		}
		
		if ((same2 == true) && (!words[1].equals(words[0]))) { // print index 1 if it was equal to any other index except 0
			System.out.println(words[1]);
		}
		
		if (same3 == true && (!words[2].equals(words[0])) && (!words[2].equals(words[1]))) { // print index 2 if it was equal to any other index except 0 and 1 
			System.out.println(words[2]);
		}
		
		if (same4 == true && (!words[3].equals(words[0])) && (!words[3].equals(words[1])) && (!words[2].equals(words[3]))) { // print index 3 if it was equal to index 4
			System.out.println(words[3]);
		}
		input.close();

	}

}
