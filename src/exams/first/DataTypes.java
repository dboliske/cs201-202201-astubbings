package exams.first;

import java.util.Scanner;

public class DataTypes {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // create scanner
		System.out.print("Enter in an integer: ");
		int in = Integer.parseInt(input.nextLine()); // get number
		int change = in + 65; // add 65
		char out = (char)change; // convert to char
		System.out.println("Result: " + out); // print
		input.close();

	}

}
