# Midterm Exam

## Total

100/100

## Break Down

1. Data Types:                  20/20
    - Compiles:                 5/5
    - Input:                    5/5
    - Data Types:               5/5
    - Results:                  5/5
2. Selection:                   20/20
    - Compiles:                 5/5
    - Selection Structure:      10/10
    - Results:                  5/5
3. Repetition:                  20/20
    - Compiles:                 5/5
    - Repetition Structure:     5/5
    - Returns:                  5/5
    - Formatting:               5/5
4. Arrays:                      21/20
    - Compiles:                 5/5
    - Array:                    5/5
    - Exit:                     5/5
    - Results:                  6/5
5. Objects:                     19/20
    - Variables:                5/5
    - Constructors:             5/5
    - Accessors and Mutators:   5/5
    - toString and equals:      4/5

## Comments

1. Perfect
2. Perfect
3. Perfect
4. Perfect, even gave you an extra point since you correctly only printed words once.
5. Nearly perfect, but your `equals` method doesn't follow the UML diagram exactly and take an Object as the parameter.
