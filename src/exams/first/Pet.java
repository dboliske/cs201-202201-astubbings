package exams.first;

public class Pet {
	
	private String name;
	private int age;
	
	public Pet() { // default constructor
		name = "Cookie";
		age = 3;
	}
	
	public Pet(String name, int age) { // non-default constructor
		setName(name);
		setAge(age);
	}
	
	public void setName(String name) { // set name
		this.name = name;
	}
	
	public void setAge(int age) { // set age only if 0 or greater
		if (age >= 0) {
			this.age = age;
		}
	}
	
	public String getName() {
		return name; // get name
	}
	
	public int getAge() {
		return age; // get date
	}
	
	public boolean equals(Pet p) {
		if (!this.name.equals(p.getName())){ // check if name is the same
			return false;
		} else if (this.age != p.getAge()) { // check if age is the same
			return false;
		}
		return true; // return true if both are the same
	}
	
	public String toString() {
		return name + ", " + age; // create string
	}
}
