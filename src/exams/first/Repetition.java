package exams.first;

import java.util.Scanner;

public class Repetition {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Enter in an integer: ");
		int in = Integer.parseInt(input.nextLine()); // get size
		
		for (int i=1; i<=in; i++) { // create rows
			for (int k=1; k<=i; k++) { // create columns
				System.out.print("* ");
			}
			System.out.println();
		}
		
		input.close();

	}

}
