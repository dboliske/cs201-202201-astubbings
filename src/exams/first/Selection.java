package exams.first;

import java.util.Scanner;

public class Selection {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // create scanner
		System.out.print("Enter in an integer: ");
		int in = Integer.parseInt(input.nextLine()); // get number
		if ((in % 2 == 0) && (in % 3 == 0)) { // if divisible by 2 and 3
			System.out.println("foobar");
		} else if (in % 2 == 0) { // if divisible by 2
			System.out.println("foo");
		} else if (in % 3 == 0) { // if divisible by 3
			System.out.println("bar");
		}
		input.close();

	}

}
