// Avery Stubbings, CS-201-01, Apr. 29, 2022, Circle
package exams.second;

public class Circle extends Polygon { // inherits from polygon
	
	private double radius;
	
	// default-constructor
	public Circle() {
		super();
		radius = 1;
	}
	
	// setter
	public void setRadius(double radius) {
		if (radius > 0) {
			this.radius = radius;
		}
	}
	
	// getter
	public double getRadius() {
		return radius;
	}
	
	// prints info as string
	public String toString() {
		return "This is a circle of radius " + radius + " called " + getName() + ".";
	}
	
	// calculates area of a circle
	public double area() {
		return Math.PI * radius * radius;
	}
	
	// calculates perimeter of a circle
	public double perimeter() {
		return 2.0 * Math.PI * radius;
	}
}
