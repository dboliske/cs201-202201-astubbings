// Avery Stubbings, CS-201-01, Apr. 29, 2022, Classroom 
package exams.second;

public class Classroom {
	
	protected String building;
	protected String roomNumber;
	private int seats;
	
	// default-constructor
	public Classroom() {
		building = "Stuart Building";
		roomNumber = "104";
		seats = 150;
	}
	
	// setters
	public void setBuilding(String building) {
		this.building = building;
	}
	
	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}
	
	public void setSeats(int seats) {
		if (seats >= 0) {
			this.seats = seats;
		}
	}
	
	// getters
	public String getBuilding() {
		return building;
	}
	
	public String getRoomNumber() {
		return roomNumber;
	}
	
	public int getSeats() {
		return seats;
	}
	
	// prints info as a string
	public String toString() {
		return building + " " + roomNumber + ", " + seats + " seats";
	}

}
