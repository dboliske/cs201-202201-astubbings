// Avery Stubbings, CS-201-01, Apr. 29, 2022, ComputerLab
package exams.second;

public class ComputerLab extends Classroom { // inherits from Classroom
	
	private boolean computers;
	
	// default-constructor
	public ComputerLab() {
		super();
		computers = false;
	}
	
	// setter
	public void setComputers(boolean computers) {
		this.computers = computers;
	}
	
	// getter
	public boolean hasComputers() {
		return computers;
	}
	
	// prints info as a string
	@Override
	public String toString() {
		return getBuilding() + " " + getRoomNumber() + ", " + getSeats() + " seats. Computers " + computers + ".";   
	}
}
