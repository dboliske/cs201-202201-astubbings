// Avery Stubbings, CS-201-01, Apr. 29, 2022, List
package exams.second;

import java.util.ArrayList;
import java.util.Scanner;

public class List {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		ArrayList<Double> numbers = new ArrayList<Double>(); // create array list
		
		boolean done = false; 
		
		// flag controlled loop
		do {
			System.out.print("Enter a number or enter Done to finish: ");
			String user = input.nextLine(); // get input from user
			if (user.equalsIgnoreCase("Done")) {
				done = true;
			} else {
				try {
					numbers.add(Double.parseDouble(user)); // store valid numbers in array list
				} catch (Exception e) {
					System.out.println("You must enter a number or Done.");
				}	
			}
		} while (!done);
		
		// initialize min and max
		double min = numbers.get(0);
		double max = numbers.get(0);
		
		// loop through and find min and max
		for (int i=0; i<numbers.size(); i++) {
			if (numbers.get(i) < min) {
				min = numbers.get(i);
			}
			if (numbers.get(i) > max) {
				max = numbers.get(i);
			}
		}
		
		System.out.println("Min: " + min);
		System.out.println("Max: " + max);
		
		input.close();

	}

}
