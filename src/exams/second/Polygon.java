// Avery Stubbings, CS-201-01, Apr. 29, 2022, Polygon
package exams.second;

public abstract class Polygon { // abstract class
	
	protected String name;
	
	// default-constructor
	public Polygon() {
		name = "rectangle";
	}
	
	// setter
	public void setName(String name) {
		this.name = name;
	}
	
	// getter
	public String getName() {
		return name;
	}
	
	public String toString() {
		return "This polygon is named " + name + ".";
	}
	
	// abstract methods
	public abstract double area();
	
	public abstract double perimeter();
	
	
}
