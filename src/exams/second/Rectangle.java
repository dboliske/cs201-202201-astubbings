// Avery Stubbings, CS-201-01, Apr. 29, 2022, Rectangle
package exams.second;

public class Rectangle extends Polygon { // inherits from Polygon
	
	private double width;
	private double height;
	
	// default-constructor
	public Rectangle() {
		super();
		width = 1;
		height = 1;
	}
	
	// setters
	public void setWidth(double width) {
		if (width > 0) {
			this.width = width;
		}
	}
	
	public void setHeight(double height) {
		if (height > 0) {
			this.height = height;
		}
	}
	
	// getters
	public double getWidth() {
		return width;
	}
	
	public double getHeight() {
		return height;
	}
	
	// prints info as string
	public String toString() {
		return "This is a " + width + " x " + height + " rectangle called " + getName() + ".";
	}
	
	// calculates area of rectangle
	public double area() {
		return height * width;
	}
	
	// calculates perimeter of rectangle
	public double perimeter() {
		return 2.0 * (width + height);
	}
}
