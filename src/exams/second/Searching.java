// Avery Stubbings, CS-201-01, Apr. 29, 2022, Searching
package exams.second;

import java.util.Scanner;

public class Searching {

	public static int jumpSearch(double[] n, int prev, int step, double input) {
		
		// check is value at step/end is less than input. Ensure previous is not greater than length
		if ((n[Math.min(step, n.length - 1)] < input) && (prev < n.length)) { 
			prev = step; // adjust previous and step
			step += (int)Math.sqrt(n.length);
			return jumpSearch(n, prev, step, input); // recursive call with new previous and step
		} else {
			while ((prev < n.length) && (prev <= Math.min(step, n.length - 1))) { // loop though from previous to minimum of step/end
				if (input == n[prev]) {
	                return prev; // find input if in array
	            }
				prev++; 
			}
		}
		
		return -1; // otherwise return -1
	}	
		
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); 
		System.out.print("Enter a number from the list:"); 
		try {
			double choice = Double.parseDouble(input.nextLine()); // get user input
			double[] numbers = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
			int index = jumpSearch(numbers, 0, (int)Math.sqrt(numbers.length), choice); // run jumpSearch
			System.out.print(index);
		} catch (Exception e) {
			System.out.println("Must be a number.");
		}
		input.close();
	}

}
