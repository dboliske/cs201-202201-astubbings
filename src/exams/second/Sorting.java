// Avery Stubbings, CS-201-01, Apr. 29, 2022, Sorting
package exams.second;

public class Sorting {

	public static String[] selectionSort(String[] words) {
		for (int i=0; i<words.length - 1; i++) { // loop through array
			int first = i; // set minimum
			for (int k=i+1; k<words.length; k++) {
				if (words[k].compareTo(words[first]) < 0) {
					first = k; // find smallest number in array
				}
			}
			
			if  (first != i) {
				String temp = words[i];
				words[i] = words[first]; // Swap. Process repeats for next smallest etc.
				words[first] = temp;
			}
		}
		
		return words;
	}
	
	
	public static void main(String[] args) {
		String[] words = {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"};
		words = selectionSort(words); // sort and print words
		for (int j=0; j<words.length; j++) {
			System.out.println(words[j]);
		}
	}

}
