// Avery Stubbings, CS-201-01, Jan. 19, 2022, Square
package labs.lab0;

public class Square {
// Select the dimensions of the square, symbol used, and whether it is hollow/filled. 
// In this case it will be a 4x4, filled square using stars.
// Print a line with four stars (# of columns) with spaces between them.
// Print this line three more times (# of rows).
	
	public static void main(String[] args) {
		
		System.out.println("* * * *");
		System.out.println("* * * *");
		System.out.println("* * * *");
		System.out.println("* * * *");
	}

}
// Very close to desired result.
// Horizontal and vertical spaces are slightly different.
// Therefore it is not quite a perfect square.
