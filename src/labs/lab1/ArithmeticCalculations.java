// Avery Stubbings, CS-201-01, Jan. 24, 2022, ArithmeticCalculations (2)
package labs.lab1;

public class ArithmeticCalculations {
	public static void main(String[] args) {
		
		System.out.println(54-18); // father's age subtract my age
		System.out.println(2003*2); // birth year times 2
		System.out.println(70*2.54); // height in inches to centimeters
		System.out.println(70/12 + " Feet " + 70%12 + " Inches "); // height in inches to feet and inches
		
		
	}
	
}
