// Avery Stubbings, CS-201-01, Jan. 24, 2022, BoxSquareFeet (5)
package labs.lab1;

import java.util.Scanner;

public class BoxSquareFeet {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // create scanner
		
		System.out.println("Enter a box length (inches): "); // prompting user and getting dimensions from user
		double length = Double.parseDouble(input.nextLine());
		System.out.println("Enter a box width (inches): ");
		double width = Double.parseDouble(input.nextLine());
		System.out.println("Enter a box depth (inches): ");
		double depth = Double.parseDouble(input.nextLine());
		input.close(); // close scanner
		
		if (length >0 && width >0 && depth > 0) { // check for positive values (dimensions can't be negative)
			double SurfaceAreaInches = 2*(width*length+depth*length+depth*width); // calculating surface area in inches
			double SurfaceAreaSquareFeet = SurfaceAreaInches/144; // converting to square feet
		
			System.out.println("The amount of wood needed to make the box is " + SurfaceAreaSquareFeet + " square feet."); // display surface area
		}
		else {
			System.out.println("Can't have negative input.");
		}
	}

}
// Test					Expected 		Answer 
// 10, 10, 10			4.167			4.167..
// 11.23, 14.2, 3.77	3.546			3.546..
// 2.3, 5, 7.8			0.95			0.95..
// -2, 4, 6				Error			Negative (Not Valid Input)
// Answers are good.