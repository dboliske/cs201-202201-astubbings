// Avery Stubbings, CS-201-01, Jan. 24, 2022, EchoName (1)
package labs.lab1;

import java.util.Scanner;

public class EchoName {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // import a scanner for user input
		
		System.out.print("Enter your name: "); // prompt user
		String name = input.nextLine(); // storing user's name
		
		input.close(); // close scanner
		
		System.out.println(name); // display user's name
	}

}
