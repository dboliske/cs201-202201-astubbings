// Avery Stubbings, CS-201-01, Jan. 24, 2022, FahrenheitCelsius (4)
package labs.lab1;

import java.util.Scanner;

public class FahrenheitCelsius {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // create scanner for user input
		
		System.out.println("Enter a temperature in degrees Fahrenheit: "); // prompt user
		double FahrenheitTemp = Double.parseDouble(input.nextLine()); // get Fahrenheit Temperature from user
		double CelsiusTemp = (FahrenheitTemp-32)*((double)5/(double)9); // calculate and store Celsius Temperature
		System.out.println("The temperature in degrees Celsius is " + CelsiusTemp); // display Celsius Temperature
		
		System.out.println("Enter a temperature in degrees Celsius: "); // prompt user
		double CelsiusTemp2 = Double.parseDouble(input.nextLine()); // get Celsius Temperature from user
		double FahrenheitTemp2 = CelsiusTemp2*((double)9)/((double)5) + 32; // calculate and store Fahrenheit Temperature
		System.out.println("The temperature in degrees Fahrenheit is " + FahrenheitTemp2); // display Fahrenheit Temperature
		
		input.close(); // close scanner
	}

}
// Test			Expected		Answer
// -33, -25.2   -36.11, -13.36	-36.11.., -13.36
// 36, 15		2.22, 59		2.22.., 59
// 98.3, 32.4	36.83, 90.32	36.83.., 90.32
// 67.77, 0		19.87, 32		19.87.., 32
// Answers are good