// Avery Stubbings, CS-201-01, Jan. 24, 2022, GettingCharacter (3)
package labs.lab1;

import java.util.Scanner;

public class GettingCharacter {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // create scanner
		
		
		System.out.println("Enter your first name: "); // prompt user
		String name = input.nextLine(); // get name from user
		System.out.println(name.charAt(0)); // displays first initials
		
		input.close(); // close scanner

	}

}
