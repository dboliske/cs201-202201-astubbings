// Avery Stubbings, CS-201-01, Jan. 24, 2022, InchesToCentimeters (6)
package labs.lab1;

import java.util.Scanner;

public class InchesToCentimeters {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // create scanner
		
		System.out.println("Enter the number of inches: "); // prompt user
		double inches = Double.parseDouble(input.nextLine()); // get inches from user
		double centimeters = inches*2.54; // convert to centimeters
		
		System.out.println("That length is equal to " + centimeters + " centimeters."); // display centimeters
		
		input.close(); // close scanner

	}

}
// Test		Expected 	Answer
// 144.3	366.5		366.5
// 1.67		4.2418		4.2418
// 18.99	48.23		48.23
// 14.2		36.068		36.068
// Answers are good.