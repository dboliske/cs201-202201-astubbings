// Avery Stubbings, CS-201-01, Jan. 31, 2022, AverageGrade (2)
package labs.lab2;

import java.util.Scanner;

public class AverageGrade {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // create scanner
		System.out.println("Enter a grade or enter -1 to finish: "); // prompt user
		double grade = Double.parseDouble(input.nextLine()); // get initial grade
		double TotalGrade = 0; // set grade count
		double Counter = 0; // set number of inputs count
		
		while (grade != -1) { // while loop (grade does not equal -1)
			TotalGrade = TotalGrade + grade; // add grade to total
			Counter = Counter + 1; // count number of inputs
			System.out.println("Enter a grade or enter -1 to finish: ");
			grade = Double.parseDouble(input.nextLine()); // get next grade
		}
		
		input.close(); // close scanner
		double average = TotalGrade / Counter; // calculate average
		System.out.println("The calculated average is " + average); // print average
	}

}
