// Avery Stubbings, CS-201-01, Jan. 31, 2022, Menu (3)
package labs.lab2;

import java.util.Scanner;

public class Menu {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // create scanner
		boolean running = true;
		
		while (running) { 
			System.out.println("1. Say Hello"); // Give user menu options
			System.out.println("2. Addition");
			System.out.println("3. Multiplication");
			System.out.println("4. Exit");
			String choice = input.nextLine();
			
			switch(choice) {
				case "1": // case 1
					System.out.println("Hello"); // print hello
					break;
				case "2": // case 2
					System.out.println("Enter a number: "); 
					double num1 = Double.parseDouble(input.nextLine()); 
					System.out.println("Enter another number: ");
					double num2 = Double.parseDouble(input.nextLine()); // get two numbers
					double addition = num1 + num2; // add the two numbers
					System.out.println("The addition of those two numbers is " + addition); // display sum
					break;
				case "3": // case 3
					System.out.println("Enter a number: "); 
					double num3 = Double.parseDouble(input.nextLine()); 
					System.out.println("Enter another number: ");
					double num4 = Double.parseDouble(input.nextLine()); // get two numbers
					double multiplication = num3 * num4; // multiply the two numbers
					System.out.println("The multiplication of those two numbers is " + multiplication); // display product
					break;
				case "4": // case 4
					running = false; // change flag and end loop
					break;
				default:
					System.out.println("That was not an option. Please try again."); // prints message if other character typed in
					break;
			}
			
		}
		
		System.out.println("Goodbye"); // let user know program is done
		input.close(); // close scanner
	}

}
