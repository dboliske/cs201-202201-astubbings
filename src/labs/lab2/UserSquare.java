// Avery Stubbings, CS-201-01, Jan. 31, 2022, UserSquare (1)
package labs.lab2;

import java.util.Scanner;

public class UserSquare {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // create scanner
		System.out.println("Enter an integer value for the dimensions of your square:"); // prompt user
		int dimensions = Integer.parseInt(input.nextLine()); // get dimension
		
		for (int k=1; k <= dimensions; k++) { // nested for loop
			for (int h=1; h <= dimensions; h++) {
				System.out.print("* "); // prints row with specified dimension
				
			}
				System.out.println(); // go to next row
			}
			
		input.close(); // close scanner
	}
		
		
}
