// Avery Stubbings, CS-201-01, Feb. 9, 2022, CalculateAverage (1)
package labs.lab3;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class CalculateAverage {

	public static void main(String[] args) {
		try {
			File f  = new File("src/labs/lab3/grades.csv"); // file reader
			Scanner input = new Scanner(f);
		
			double[] grades = new double[14]; // create array
			int count = 0; // create count
			while(input.hasNextLine()) {
				String[] values = input.nextLine().split(","); // split numbers from text
				grades[count] = Double.parseDouble(values[1]); // store numbers into grades array
				count++;
		}
			double Total = 0;
			for (int i=0; i < grades.length; i++) {
				Total = Total + grades[i]; // add each element of array
		}
		
			double average = Total/(grades.length); // calculate average of array
			System.out.println("The average grade is: " + average); // print average
			input.close();
		
		} catch (IOException e) { // catch IOException
			System.out.print("Error. Unable to read file.");
		}
		
	}

}
