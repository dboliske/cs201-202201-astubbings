// Avery Stubbings, CS-201-01, Feb. 9, 2022, Minimum (3)
package labs.lab3;

public class Minimum {

	public static void main(String[] args) {
		// create array
		int[] values = {72, 101, 108, 108, 111, 32, 101, 118, 101, 114, 121, 111, 110, 101, 33, 32, 76, 111, 111, 107, 32, 97, 116, 32, 116, 104, 101, 115, 101, 32, 99, 111, 111, 108, 32, 115, 121, 109, 98, 111, 108, 115, 58, 32, 63264, 32, 945, 32, 8747, 32, 8899, 32, 62421};
		int minimum = values[0]; // set minimum as first index
		for (int i=0; i<values.length; i++) { // check each index
			if (values[i] < minimum) {
				minimum = values[i]; // change minimum if new index is smaller
			}
		}
		System.out.println("The minimum value in the array is: " + minimum); // print minimum
	}

}
