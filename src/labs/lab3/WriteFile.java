// Avery Stubbings, CS-201-01, Feb. 9, 2022, WriteFile (2)
package labs.lab3;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class WriteFile {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // create scanner for user input
		double[] numbers = new double[1]; // create array
		boolean flag = false; // create flag variable
		int count = 0; 
		String check = "Done"; // variable to check done later
		
		do { // flag control loop
			try {
				System.out.print("Enter a number or enter Done when finished: "); // prompt user for number
				String checkdone = input.nextLine();
				if (check.equalsIgnoreCase(checkdone)){ // check if they entered Done end loop
					flag = true;
				} else {
					numbers[count] = Double.parseDouble(checkdone);	// put number in array
					count++;
					// resize array
					if (count == numbers.length) {
						double[] bigger = new double[1 + numbers.length];
						for (int i=0; i<numbers.length; i++) {
							bigger[i] = numbers[i];
						}
						numbers = bigger;
						bigger = null;	
					}
				}
			} catch (Exception e) { // catch invalid inputs
				System.out.println("Invalid input. Please enter a number.");
			}
		} while (!flag);
		
		System.out.println("Now enter a file name: "); // prompt for file name
		String filename = input.nextLine(); // get file name
		input.close();
		
		try {
			FileWriter f = new FileWriter("src/labs/lab3/" +filename); // write file
			for  (int k=0; k<(numbers.length-1); k++) {
				f.write(numbers[k] + "\n"); // write every line except last one to avoid writing extra zero
			}
			f.flush();
			f.close();
			System.out.println("Your file has successfully been written."); // Let user know file is complete
		} catch (IOException e) { // catch IOException
			System.out.println("Error writing file.");
		}
		
		
	}

}
