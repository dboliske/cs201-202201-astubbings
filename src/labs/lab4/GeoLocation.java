package labs.lab4;

public class GeoLocation {
	
	private double lat;
	private double lng;
	
	public GeoLocation() {  // default constructor
		lat = 0;
		lng = 0;
	}
	
	public GeoLocation(double latitude, double longitude) { // non-default constructor
		lat = 0;
		setLat(latitude);
		lng = 0;
		setLng(longitude);
	}
	
	public double getLat() { // accessors
		return lat;
	}
	
	public double getLng() {
		return lng;
	}
	
	public void setLat(double lat) { // mutator
		this.lat = lat;
	}
	
	public void setLng(double lng) {
		this.lng = lng;
	}
	
	public String toString() {
		return "(" + lat + ", " + lng + ")";
	}
	
	public boolean validLat() {
		if (this.lat >= -90 && this.lat <= 90) {
			return true;
		}
		return false;
	}
	
	public boolean validLng() {
		if (this.lng >=-180 && this.lng <=180) {
			return true;
		}
		return false;
	}
	
	public boolean equals(GeoLocation l) {
		if (this.lat != l.getLat()) {
			return false;
		} else if (this.lng != l.getLng()) {
			return false;
		}
		return true;
	}
}

