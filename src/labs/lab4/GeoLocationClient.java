package labs.lab4;

public class GeoLocationClient {

	public static void main(String[] args) {
		
		GeoLocation g1 = new GeoLocation();
		GeoLocation g2 = new GeoLocation(45.67, 100.35);
		
		System.out.println("Latitude: " + g1.getLat() + " Longitude: " + g1.getLng()); // print lat and lng using accessors

		System.out.println("Latitude: " + g2.getLat() + " Longitude: " + g2.getLng());
	}

}
