# Lab 4

## Total

27.75/30

## Break Down

* Part I GeoLocation
  * Exercise 1-9        7.75/8
  * Application Class   1/1
* Part II PhoneNumber
  * Exercise 1-9        7.75/8
  * Application Class   1/1
* Part III 
  * Exercise 1-8        7.75/8
  * Application Class   1/1
* Documentation         1.5/3

## Comments
For each part, you do not initialize the variables to default values in the non-default constructor.
You just needed to call the setters. Other than that, everything else looks good.
Documentation not quite there