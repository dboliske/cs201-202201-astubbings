package labs.lab4;

public class PhoneNumber {

	private String countryCode;
	private String areaCode;
	private String number;
	
	public PhoneNumber() { // default constructor
		countryCode = "1";
		areaCode = "312";
		number = "8907833";
	}
	
	public PhoneNumber(String cC, String aC, String n) { // non-default constructor
		countryCode = "1";
		setCountryCode(cC);
		areaCode = "312";
		setAreaCode(aC);
		number = "8907833";
		setNumber(n);
	}
	
	public String getCountryCode() { // accessors
		return countryCode;
	}
	
	public String getAreaCode() {
		return areaCode;
	}
	
	public String getNumber() {
		return number;
	}
	
	public void setCountryCode(String countryCode) { // mutator
		this.countryCode = countryCode;
	}
	
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	
	public void setNumber(String number) {
		this.number = number;
	}
	
	public String toString() {
		return countryCode + "-" + areaCode + "-" + number;
	}
	
	public boolean validAreaCode() {
		if (areaCode.length() == 3) {
			return true;
		}
		return false;
	}
	
	public boolean validNumber() {
		if (number.length() == 7) {
			return true;
		}
		return false;
	}
	
	public boolean equals(PhoneNumber n) {
		if (!this.countryCode.equals(n.getCountryCode())) {
			return false;
		} else if (!this.areaCode.equals(n.getAreaCode())) {
			return false;
		} else if (!this.number.equals(n.getNumber())) {
			return false;
		}
		return true;
	}

}
