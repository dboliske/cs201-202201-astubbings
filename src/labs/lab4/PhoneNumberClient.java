package labs.lab4;

public class PhoneNumberClient {

	public static void main(String[] args) {
		PhoneNumber p1 = new PhoneNumber();
		PhoneNumber p2 = new PhoneNumber("2", "456", "9675890");
		
		System.out.println(p1.toString()); // print using to String method
		System.out.println(p2.toString());
	}

}
