package labs.lab4;

public class Potion {
	
	private String name;
	private double strength;
	
	public Potion() { // default constructor
		name = "polyjuice";
		strength = 2;
	}
	
	public Potion(String name1, double strength1) { // non-default constructor
		name = "polyjuice";
		setName(name1);
		strength = 2;
		setStrength(strength1);
	}
	
	public String getName() { // accessors
		return name;
	}
	
	public double getStrength() {
		return strength;
	}
	
	public void setName(String name) { // mutator
		this.name = name;
	}
	
	public void setStrength(double strength) { 
		this.strength = strength;
	}
	
	public String toString() {
		return name + ", " + strength;
	}
	
	public boolean validStrength() {
		if (strength >= 0 && strength <= 10) {
			return true;
		}
		return false;
	}
	
	public boolean equals(Potion p) {
		if (!this.name.equalsIgnoreCase(p.getName())) {
			return false;
		} else if (this.strength != p.getStrength()) {
			return false;
		}
		return true;
	}
	
}

