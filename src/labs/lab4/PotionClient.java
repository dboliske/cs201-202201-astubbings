package labs.lab4;

public class PotionClient {

	public static void main(String[] args) {
		Potion p1 = new Potion();
		Potion p2 = new Potion("Fire Breath", 10);
		
		System.out.println(p1.toString()); // print using to String Method
		System.out.println(p2.toString());
		
	}

}
