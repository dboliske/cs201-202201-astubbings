// Avery Stubbings, CS-201-01, March 7, 2022, CTAStation
package labs.lab5;

public class CTAStation extends GeoLocation { // inherits from GeoLocation
	
	private String name;
	private String location;
	private boolean wheelchair;
	private boolean open;
	
	public CTAStation() { // default constructor 
		super();
		name = "Morgan";
		location = "elevated";
		wheelchair = true;
		open = true;	
	}
	
	public CTAStation(String name, double lat, double lng, String location, boolean wheelchair, boolean open) { // non-default constructor
		super(lat, lng);
		setName(name);
		setLocation(location);
		setWheelchair(wheelchair);
		setOpen(open);
	}
	
	public String getName() { // accessors
		return name;
	}
	
	public String getLocation() {
		return location;
	}
	
	public boolean hasWheelchair() {
		return wheelchair;
	}
	
	public boolean isOpen() {
		return open;
	}
	
	public void setName(String name) { // mutators
		this.name = name;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public void setWheelchair(boolean wheelchair) {
		this.wheelchair = wheelchair;
	}
	
	public void setOpen(boolean open) {
		this.open = open;
	}
	
	@Override
	public String toString() {
		return name + ", " + super.toString() + ", " + location + ", " + wheelchair + ", " + open; // prints out variables as csv
	} // overrides toString from GeoLocation
	
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) { // check if equal to super-class
			return false;
		} else if (!(obj instanceof CTAStation)) { // check if object is instance of CTAStation
			return false;
		}
		
		CTAStation cta = (CTAStation)obj; // type cast object to CTAStation
		if (!(this.name.equals(cta.getName()))) { // check if name is equal
			return false;
		} else if (!(this.location.equals(cta.getLocation()))) { // check if location is equal 
			return false;
		} else if (this.wheelchair != cta.hasWheelchair()) { // check if wheelchair is equal
			return false;
		} else if (this.open != cta.isOpen()) { // check if open is equal
			return false;
		} 
			
		return true;
		
	}
	
}
