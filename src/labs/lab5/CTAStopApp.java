// Avery Stubbings, CS-201-01, March 7, 2022, CTAStopApp
package labs.lab5;

import java.io.File;
import java.util.Scanner;

public class CTAStopApp {

	
	public static CTAStation[] readFile(String filename) { // read file method
		CTAStation[] stations = new CTAStation[34]; // create array to store data
		int count = 0; // make count
		try {
			File f = new File(filename);
			Scanner input = new Scanner(f);
			while (input.hasNextLine()) { // loop through file
				String[] info = input.nextLine().split(","); // split items in file so it can be passed into CTAStation()
				if ((info[4].equalsIgnoreCase("true")) || (info[4].equalsIgnoreCase("false"))) { // this ensures that the first title line is skipped
					CTAStation s = new CTAStation(info[0], Double.parseDouble(info[1]), Double.parseDouble(info[2]),
							info[3], Boolean.parseBoolean(info[4]), Boolean.parseBoolean(info[5])); // create instance of CTAStation
					stations[count] = s; // store instance in array
					count++;
				} 
			}
			input.close();
		} catch (Exception e) {
			System.out.println("Error reading file.");
		}
		return stations;
		
	}

	
	public static CTAStation[] menu(CTAStation[] stations) { // menu method
		Scanner input = new Scanner(System.in);
		boolean done = false;
		do {
			System.out.println("1. Display Station Names"); // prompt user
			System.out.println("2. Display Stations with/without Wheelchair Access");
			System.out.println("3. Display Nearest Station");
			System.out.println("4. Exit");
			String choice = input.nextLine(); // get choice
			
			switch (choice) {
			case "1": 
				displayStationNames(stations); // call in displayStationNames method
				break;
			case "2":
				displayByWheelchair(input, stations); // call in displayByWheelchair method
				break;
			case "3":
				displayNearest(input, stations); // call in displayNearest method
				break;
			case "4":
				done = true;
				break;
			default: 
				System.out.println("That was not an option. Please try again.");
			}	
		} while (!done);
		input.close();
		
		return stations;
	}
	
	public static CTAStation[] displayStationNames(CTAStation[] stations) { // displayStationNames method
		for (int k=0; k<stations.length; k++) {
			System.out.println(stations[k].getName()); // loops through and prints names of stations
		}
		return stations;
	}
	
	public static CTAStation[] displayByWheelchair(Scanner input, CTAStation[] stations) { // displayByWheelchair method
		boolean done = false;
		do {
			System.out.println("Enter in y or n"); // prompt user for yes or no wheel chair
			char in = input.nextLine().charAt(0);
			switch (in) {
			case 'y':
			case 'Y':
				int count = 0;
				for (int k=0; k<stations.length; k++) {
					if (stations[k].hasWheelchair() == true) { // check if it has a wheelchair
						System.out.println(stations[k].getName()); // prints stations
						count++;
					}
				}
				if (count == 0) { // a zero count means no stations have wheelchair access
					System.out.println("No stations found with matching criteria.");
				}
				done = true; // end loop
				break;
			case 'n':
			case 'N':
				int count1 = 0;
				for (int k=0; k<stations.length; k++) {
					if (stations[k].hasWheelchair() == false) { // check if it does not have a wheelchair
						System.out.println(stations[k].getName()); // prints stations
						count1++;
					}
				}
				if (count1 == 0) {
					System.out.println("No stations found with matching criteria.");
				}
				done = true;
				break;
			default:
				System.out.println("That was not an option. Please try again.");
			}
		} while (!done);
		return stations;
	}
	
	public static CTAStation[] displayNearest(Scanner input, CTAStation[] stations) { // displayNearest method
		try {
		System.out.print("Enter a latitude:"); // get lat and lng from user
		double lat = Double.parseDouble(input.nextLine());
		System.out.print("Enter a longitude:");
		double lng = Double.parseDouble(input.nextLine());
		double closest = stations[0].calcDistance(lat, lng); // initialize element zero as closest
		int count = 0; // initialize count
		for (int k=0; k<stations.length; k++) {
			double distance = stations[k].calcDistance(lat, lng); // loop through and calculate distance
			if (distance < closest) {
				closest = distance; // if smaller distance replace closest and save location in array with count 
				count = k;  
			}
		}
		System.out.println("The nearest station is: " + stations[count].getName()); // print closest station
		} catch (Exception e) {
			System.out.println("Invalid input."); // catch invalid inputs
		}
		
		return stations;
	}
	
	public static void main(String[] args) { // main
		CTAStation[] stations = readFile("src/labs/lab5/CTAStops.csv"); // call read file method
		stations = menu(stations); // pass data to menu
		System.out.println("Goodbye");
	}

}
