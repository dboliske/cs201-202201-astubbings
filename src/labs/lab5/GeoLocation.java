// Avery Stubbings, CS-201-01, March 7, 2022, GeoLocation
package labs.lab5;

public class GeoLocation {
	
	private double lat;
	private double lng;
	
	public GeoLocation() {  // default constructor
		lat = 0;
		lng = 0;
	}
	
	public GeoLocation(double latitude, double longitude) { // non-default constructor
		setLat(latitude);
		setLng(longitude);
	}
	
	public double getLat() { // accessors
		return lat;
	}
	
	public double getLng() {
		return lng;
	}
	
	public void setLat(double lat) { // mutator
		if (lat >= -90 && lat <= 90) { // verify valid latitude
			this.lat = lat;
		}
	}
	
	public void setLng(double lng) {
		if (lng >= -180 && lng <= 180) { // verify valid longitude
			this.lng = lng;
		}
	}
	
	public String toString() {
		return lat + ", " + lng; // prints latitude and longitude in comma format
	}
	
	public boolean validLat() {
		if (this.lat >= -90 && this.lat <= 90) { // returns true if between -90 and 90
			return true;
		}
		return false; // returns false otherwise
	}
	
	public boolean validLng() {
		if (this.lng >=-180 && this.lng <=180) { // returns true if between -180 and 180
			return true;
		}
		return false; // returns false otherwise
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof GeoLocation)) { // check if object is an instance of GeoLocation
			return false;
		}
		
		GeoLocation g = (GeoLocation)obj; // type cast object
		if (this.lat != g.getLat()) { // check for same latitude
			return false;
		} else if (this.lng != g.getLng()) { // check for same longitude
			return false;
		}
		return true;
	}
	
	public double calcDistance(GeoLocation g) {
		return Math.sqrt(Math.pow(this.lat - g.getLat(), 2) + Math.pow(this.lng - g.getLng(), 2)); // accepts GeoLocation
	}
	
	public double calcDistance(double lat, double lng) {
		return Math.sqrt(Math.pow(this.lat - lat, 2) + Math.pow(this.lng - lng, 2)); // accepts two doubles
	}
}

