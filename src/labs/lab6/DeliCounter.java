// Avery Stubbings, CS-201-01, March 21, 2022, DeliCounter
package labs.lab6;

import java.util.ArrayList;
import java.util.Scanner;

public class DeliCounter {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); 
		ArrayList<String> customers = new ArrayList<String>(); // create array list
		menu(input, customers); // pass scanner and array list to menu
		System.out.println("Goodbye!");
	
	}
	
	public static ArrayList<String> menu(Scanner input, ArrayList<String> customers){
		boolean done = false;
		
		do {
			System.out.println("1. Add customer to queue"); // print options
			System.out.println("2. Help customer");
			System.out.println("3. Exit");
			String choice = input.nextLine(); // get choice from user
			
			switch (choice) {
			case "1":
				addCustomer(input, customers); // case 1 add customer method
				break;
			case "2":
				helpCustomer(customers); // case 2 help customer method
				break;
			case "3":
				done = true; // change flag variable and end loop
				break;
			default:
				System.out.println("That was not an option. Please try again");	
			}
		} while (!done);
		
		return customers;
	}
	
	public static ArrayList<String> addCustomer(Scanner input, ArrayList<String> customers){
		System.out.println("Enter in a customer's name to add to the line:"); // prompt user for name
		String name = input.nextLine();
		customers.add(name); // add name to array list
		System.out.println(name + " is at spot " + (customers.size()-1) + " in line."); // print out name and location in line
		return customers;
	}
	
	public static ArrayList<String> helpCustomer(ArrayList<String> customers){
		try {
			System.out.println(customers.get(0) + " has been helped and removed from the line."); // let user know a customer was removed
			customers.remove(0); // remove customer
		} catch (IndexOutOfBoundsException e) {
			System.out.println("There is no one in line to help."); // catch IndexOutOfBoundsException
		} catch (Exception e) {
			System.out.println("An error occurred.");
		}
		return customers;
	}
	

}
