// Avery Stubbings, CS-201-01, April 4, 2022, BinarySearch
package labs.lab7;

import java.util.Scanner;

public class BinarySearch {

	public static int binarySearch(String[] languages, int start, int end, String word) { // takes in string array, start index, end index, and desired word
		if (start != end) {
			int middle = (start + (end))/2; // set middle index
			
			if (languages[middle].equalsIgnoreCase(word)) { // check if user's word is at middle index
				return middle; 
			}
			
			if ((languages[middle].compareToIgnoreCase(word)<0)) { // check if middle word comes before user's word
				return binarySearch(languages, middle + 1, end, word); // recursive call changing start to middle + 1
			}
			
			if ((languages[middle].compareToIgnoreCase(word)>0)) { // check if middle word comes after user's word
				return binarySearch(languages, start, middle, word); // recursive call changing end to middle
			}
		}
		return -1; // return -1 if not found
	}
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // create scanner
		System.out.print("Enter in a language from the list:"); 
		String word = input.nextLine(); // get word from user
		String[] languages = {"c", "html", "java", "python", "ruby", "scala"};
		
		int index = binarySearch(languages, 0, languages.length, word); // call binarySearch
		if (index == -1) {
			System.out.println(word + " not found."); // print if not found
		} else {
			System.out.println(word + " found at index " + index + "."); // print index if found
		}
		
		
		input.close();

	}

}
