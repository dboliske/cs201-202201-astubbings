// Avery Stubbings, CS-201-01, April 4, 2022, BubbleSort
package labs.lab7;

public class BubbleSort {

	
	public static int[] bubbleSort(int[] numbers) {
boolean done = false; // set flag variable
		
		do {
			done = true;
			for (int i=0; i < numbers.length - 1; i++) { // loop through array
				if (numbers[i+1] < numbers[i]) { // check if the selected number from the loop is less than the number on its left
					int temp = numbers[i+1];
					numbers[i+1] = numbers[i]; // if so, swap the numbers and set flag to false to check again
					numbers[i] = temp;
					done = false;
				}
			}
		} while (!done);
		
		return numbers;
	}
	
	public static void main(String[] args) {
		int[] numbers = {10, 4, 7, 3, 8, 6, 1, 2, 5, 9};
		
		numbers = bubbleSort(numbers); // call bubbleSort
		
		for (int k=0; k < numbers.length; k++) {
			System.out.print(numbers[k] + " "); // print sorted numbers
		}

	}

}
