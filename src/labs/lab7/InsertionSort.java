// Avery Stubbings, CS-201-01, April 4, 2022, InsertionSort
package labs.lab7;

public class InsertionSort {

	public static String[] insertionSort(String[] words) {
		for (int k=1; k<words.length; k++) { // loop through array
			int i = k;
			while (i > 0 && words[i].compareTo(words[i-1]) < 0) { // check if the selected word comes before the left word
				String temp = words[i-1]; //  if so, swap the word places
				words[i-1] = words[i]; 
				words[i] = temp;
				i--; // decrement i to check the selected word with the next word on its left
			}
		}
		
		return words;
	}
	
	public static void main(String[] args) {
		String[] words = {"cat", "fat", "dog", "apple", "bat", "egg"};
		
		words = insertionSort(words); // call insertionSort
		
		for (int k=0; k < words.length; k++) {
			System.out.print(words[k] + " "); // print out sorted words
		}

	}

}
