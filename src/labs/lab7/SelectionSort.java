// Avery Stubbings, CS-201-01, April 4, 2022, SelectionSort
package labs.lab7;

public class SelectionSort {

	public static double[] selectionSort(double[] numbers) {
		for (int i=0; i<numbers.length - 1; i++) { // loop through numbers
			int min = i; // set minimum
			for (int j=i+1; j<numbers.length; j++) {
				if (numbers[j] < numbers[min]) { // find the minimum value in the array
					min = j;
				}
			}
			
			if (min != i) {
				double temp = numbers[min];
				numbers[min] = numbers[i]; // place the minimum value at its corresponding index. Then process repeats
				numbers[i] = temp;
			}
				
		}
		
		return numbers;
	}
	
	public static void main(String[] args) {
		double[] numbers = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};
		
		numbers = selectionSort(numbers); // call selectionSort
		
		for (int k=0; k < numbers.length; k++) {
			System.out.print(numbers[k] + " "); // print sorted numbers
		}

	}

}
