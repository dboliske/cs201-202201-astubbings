// Avery Stubbings, CS-201-01, April 29, 2022, AgeRestrictedItem
package project;

// The AgeRestrictedItem class allows users to create age restricted items that they can add to the store. Inherits from item.
public class AgeRestrictedItem extends Item {
	
	// create variable
	int age;
	
	// default constructor
	public AgeRestrictedItem() {
		super();
		age = 18;
	}
	
	// non-default constructor
	public AgeRestrictedItem(String name, double price, int age) {
		super(name, price);
		setAge(age);
	}
	
	// allows user to get age
	public int getAge() {
		return age;
	}
	
	// allows user to set age
	public void setAge(int age) {
		if (age >= 0)
		this.age = age;
	}
	
	// prints the information of the age restricted item as a string
	@Override
	public String toString() {
		return super.toString() + "," + age;
	}
	
	// method that checks if one age restricted item is equal to another
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) { 
			return false;
		} else if (!(obj instanceof AgeRestrictedItem)) { 
			return false;
		}
		
		AgeRestrictedItem i = (AgeRestrictedItem)obj; 
		if (this.age != i.getAge()) { 
			return false;
		}
		return true;
	}
	
	// displays message when client adds the age restricted item to the cart
	@Override
	public String cartMessage() {
		return super.cartMessage() + " You must be " + age + " to purchase this item.";
	}
	
}
