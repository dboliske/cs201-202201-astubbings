// Avery Stubbings, CS-201-01, April 29, 2022, Inventory
package project;

import java.util.ArrayList;

// The Inventory class allows the user to create an array list of items as their store inventory.
public class Inventory {
	
	// create variable
	private ArrayList<Item> stock;
	
	// default constructor
	public Inventory() {
		stock = new ArrayList<Item>();
	}
	
	// non-default constructor
	public Inventory(ArrayList<Item> stock) {
		this.stock = stock;
	}
	
	// allows the user to get the full stock
	public ArrayList<Item> getStock() {
		return stock;
	}
	
	// allows the user to set the full stock
	public void setStock(ArrayList<Item> stock) {
		this.stock = stock;
	}
	
	// allows the user to set the stock at a certain index
	public void setStock(int index, Item i) {
		stock.set(index, i);
	}
	
	// allows the user to get the stock at a certain index
	public Item getStock(int index) {
		return stock.get(index);
	}
	
	// prints out the contents of the inventory as a string
	public String toString() {
		try {
		String storeStock = "";
		for (int i=0; i<stock.size(); i++) {
			storeStock += stock.get(i).toString() + "\n";
		}
		
		return storeStock;
		} catch(Exception e) {
			String error = "Error printing inventory";
			return error;
		}
	}
	
	// allows the user to add an item to the inventory
	public void addItem(Item i) {
		stock.add(i);
	}
	
	// allows the user to remove an item from the inventory
	public void removeItem(int index) {
		stock.remove(index);
	}
	
	// returns the size of the inventory
	public int size() {
		return stock.size();
	}
	
	// checks if one inventory is equal to another inventory
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		} else if (! (obj instanceof Inventory)) {
			return false;
		}
		
		Inventory i = (Inventory) obj;
		if (stock.size() != i.size()) {
			return false;
		}
		
		boolean same = true;
		for (int k=0; k<stock.size(); k++) {
			if (!stock.get(k).equals(i.getStock(k))) {
				same = false;
			}
		}
		
		if (same == false) {
			return false;
		}
		
		return true;
	}
	
	// method that sorts the inventory by name using insertion sort algorithm
	public void sortStock() {
		for (int k=1; k<stock.size(); k++) { 
			int i = k;
			while (i > 0 && stock.get(i).getName().compareToIgnoreCase(stock.get(i-1).getName()) < 0) { 
				Item temp = stock.get(i-1);
				Item temp2 = stock.get(i);
				stock.set(i-1, temp2);
				stock.set(i, temp);
				i--; 
			}
		}
		
	}
	
	// method that searches for the names of items in the inventory. *** Note that this only works if the inventory is sorted
	public int searchStock(String name, int start, int end) {
		if (start != end) {
			int middle = (start + (end))/2; 
			
			if (stock.get(middle).nameEquals(name)) { 
				return middle; 
			}
			
			if ((stock.get(middle).getName().compareToIgnoreCase(name)<0)) { 
				return searchStock(name, middle + 1, end); 
			}
			
			if ((stock.get(middle).getName().compareToIgnoreCase(name)>0)) { 
				return searchStock(name, start, middle); 
			}
		}
		return -1; // return -1 if not found
	}
	
	// method that calculates the total cost of the inventory
	public double calcTotal() {
		double total = 0;
		for (int i=0; i<stock.size(); i++) {
			if (stock.get(i) != null) {
				total = total + stock.get(i).getPrice();
			}
		}
		int n = 2;
		total = Math.round(total*Math.pow(10,n))/Math.pow(10,n);
		return total;
	}
	
	
}
