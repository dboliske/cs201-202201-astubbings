// Avery Stubbings, CS-201-01, April 29, 2022, Item
package project;

// The Item class allows users to create generic items that they can add to the store.
public class Item {

	// creating variables
	private String name; 
	private double price;
	
	// default constructor
	public Item() {
		name = "banana";
		price = 0.30;
	}
	
	// non-default constructor
	public Item(String name, double price) {
		setName(name);
		setPrice(price);
	}
	
	// allows user to get name of item
	public String getName() {
		return name;
	}
	
	// allows user to set name of item
	public void setName(String name) {
		this.name = name;
	}
	
	// allows user to get price of item
	public double getPrice() {
		return price;
	}
	
	// allows user to set price of item - rounds input to two decimal places
	public void setPrice(double price) {
		int n = 2;
		if (price >= 0) {
			this.price = Math.round(price*Math.pow(10,n))/Math.pow(10,n);
		}
	}
	
	// returns the information of the object as a string
	public String toString() {
		return name + "," + price; 
	}
	
	// method that checks if one item is the same as another 
	public boolean equals(Object obj) {
		if (!(obj instanceof Item)) { 
			return false;
		}
		
		Item i = (Item)obj; 
		if (!this.name.equals(i.getName())) { 
			return false;
		} else if (this.price != i.getPrice()) { 
			return false;
		}
		return true;
	}
	
	// method that checks if the name of one item is the same as another
	public boolean nameEquals(String s) {
		if (!this.name.equalsIgnoreCase(s)) { 
			return false;
		}
		return true;
	}
	
	// displays message for when the client adds the item to the cart
	public String cartMessage() {
		return name + " which costs " + price + " dollars has been added to the cart.";
	}
	
	
}
