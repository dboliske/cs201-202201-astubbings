// Avery Stubbings, CS-201-01, April 29, 2022, ProduceItem
package project;

import java.util.regex.Pattern;

// The ProduceItem class allows users to create produce that they can add to the store. It inherits from Item class.
public class ProduceItem extends Item {
	
	// create expiration variable
	String expiration; 
	
	// create static variables of valid date formats
	private static String datePattern = "([13578]|[0][13578]|[1][02])(-|/)"+"([1-9]|[0][1-9]|[12][0-9]|[3][01])(-|/)"+"([0-9]+)";
	private static String datePattern2 = "([469]|[0][469]|[1][1])(-|/)"+"([1-9]|[0][1-9]|[12][0-9]|[3][0])(-|/)"+"([0-9]+)";
	private static String datePattern3 = "([2]|[0][2])(-|/)"+"([1-9]|[0][1-9]|[1][0-9]|[2][0-8])(-|/)"+"([0-9]+)";
	
	// default constructor
	public ProduceItem() {
		super();
		expiration = "4/29/2022";
	}
	
	// non-default constructor
	public ProduceItem(String name, double price, String exp) {
		super(name, price);
		expiration = "4/29/2022";
		setExpiration(exp);
	}
	
	// allows user to get the expiration date
	public String getExpiration() {
		return expiration;
	}
	
	// allows user to set the expiration date - must be a valid format for it to be set
	public void setExpiration(String expiration) {
		if (Pattern.matches(datePattern, expiration) || Pattern.matches(datePattern2, expiration) || Pattern.matches(datePattern3, expiration)) {
			this.expiration = expiration;
		}
	}
	
	// returns information of produce item as a string
	@Override
	public String toString() {
		return super.toString() + "," + expiration;
	}
	
	// method that checks if one produce item is equal to another
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) { 
			return false;
		} else if (!(obj instanceof ProduceItem)) { 
			return false;
		}
		
		ProduceItem i = (ProduceItem)obj; 
		if (!this.expiration.equals(i.getExpiration())) { 
			return false;
		}
		return true;
	}
	
	// displays message when client adds the produce item to the cart
	@Override
	public String cartMessage() {
		return super.cartMessage() + " Please note that this item expires on " + expiration + ".";
	}
		
}
