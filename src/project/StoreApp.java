// Avery Stubbings, CS-201-01, April 29, 2022, StoreApp
package project;

import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;
import java.util.regex.Pattern;

// The StoreApp is the client side application where the user can buy, sell, modify, and search the store.
public class StoreApp {
	
	// The main method calls the read file method and passes inventory to the menu.
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Inventory store = readFile("src/project/stock.csv");
		menu(store, input);

	}
	
	// The readFile method reads in the data from the file, creating items and storing them in an inventory.
	public static Inventory readFile(String filename) {
		Inventory store = new Inventory();
		try {
			File f = new File(filename);
			Scanner input = new Scanner(f);
			while(input.hasNextLine()) {
				String[] info = input.nextLine().split(","); // store data in array
				if (info.length == 2) {
					Item i = new Item(info[0], Double.parseDouble(info[1])); // create item if only two variables
					store.addItem(i);
				} else {
					boolean parsing = true;
					try {
						Integer.parseInt(info[2]); // try parsing element at index 2 as integer
					} catch (Exception e) {
						parsing = false;
					}
					if (parsing == true) { // if if parses create age restricted item
						AgeRestrictedItem a = new AgeRestrictedItem(info[0], Double.parseDouble(info[1]), Integer.parseInt(info[2]));
						store.addItem(a);
					} else { // if it does not parse create produce item
						ProduceItem p = new ProduceItem(info[0], Double.parseDouble(info[1]), (info[2]));
						store.addItem(p);
					}
				}
				
			}
			store.sortStock();
			input.close();
		} catch (Exception e) {
			System.out.println("Error reading file.");
		}
		return store;
	}
	
	// The writeFile method saves the data in the inventory by writing each item in CSV format to a file.
	// Items will be listed by name.
	public static void writeFile(Inventory store) {
		store.sortStock();
		try {
			FileWriter fw = new FileWriter("src/project/stock.csv");
			
			for (int i=0; i<store.size(); i++) {
				fw.write(store.getStock(i).toString() + "\n");
				fw.flush();
			}
			
			fw.close();
			
		} catch (Exception e) {
			System.out.println("Error writing file.");
		}
	}
	
	// The menu method prompts the user for a menu option and calls the method of the selected option.
	public static void menu(Inventory store, Scanner input) {
		boolean done = false;
		do {
			System.out.println("Enter a number 1 through 6 to complete one of the tasks below.");
			System.out.println("1. Add Stock");
			System.out.println("2. Sell Items");
			System.out.println("3. Search Store");
			System.out.println("4. Modify Stock");
			System.out.println("5. Print Stock");
			System.out.println("6. Save and Exit");
			System.out.print("Choice: ");
		
			String choice = input.nextLine();
			switch (choice) {
				case "1": 
					addItem(store, input);
					break;
				case "2":
					removeItem(store, input);
					break;
				case "3":
					searchStore(store, input);
					break;
				case "4":
					modifyStock(store, input);
					break;
				case "5":
					printStock(store);
					break;
				case "6":
					done = true;
					writeFile(store);
					break;
				default:
					System.out.println("That was not an option. Please try again");
			}
		} while (!done);
	}
	
	// The userPricing method gets a valid price from the user.
	public static double userPricing(Scanner input) {
		double price = -1;
		do {
			System.out.print("Enter item price: ");
			try {
				price = Double.parseDouble(input.nextLine());
			} catch (Exception e) {
				System.out.println("Price must be a positive number.");
			}
		} while (price < 0);
		return price;
	}
	
	// The userDate method gets a valid expiration date from the user.
	public static String userDate(Scanner input) {
		String datePattern = "([13578]|[0][13578]|[1][02])(-|/)"+"([1-9]|[0][1-9]|[12][0-9]|[3][01])(-|/)"+"([0-9]+)";
		String datePattern2 = "([469]|[0][469]|[1][1])(-|/)"+"([1-9]|[0][1-9]|[12][0-9]|[3][0])(-|/)"+"([0-9]+)";
		String datePattern3 = "([2]|[0][2])(-|/)"+"([1-9]|[0][1-9]|[1][0-9]|[2][0-8])(-|/)"+"([0-9]+)";
		boolean done = false;
		String expiration = "";
		do {
			System.out.print("Enter item expiration date: ");
			expiration = input.nextLine();
			if (Pattern.matches(datePattern, expiration) || Pattern.matches(datePattern2, expiration) || Pattern.matches(datePattern3, expiration)) {
				done = true;
			} else {
				System.out.println("Please enter a valid date in this format: MM/DD/YYYY");
			}
			} while (!done);
		return expiration;
	}
	
	// The userAge method gets a valid age restriction from the user.
	public static int userAge(Scanner input) {
		int age = -1;
		do {
			System.out.print("Enter an age restriction: ");
			try {
				age = Integer.parseInt(input.nextLine());
			} catch (Exception e) {
				System.out.println("Age must be an integer.");
			}
		} while (age < 0);
		return age;
	}
	
	// The addItem method allows the user to choose which type of item they want to create and enter the information needed.
	public static Inventory addItem(Inventory store, Scanner input) {
		System.out.println("Enter 1 to create an item. Enter 2 to create a produce item. Enter 3 to create an age restricted item.");
		String choice = input.nextLine(); // get the type of item from user
		switch (choice) {
		case "1": // prompt user to create and add item
			System.out.print("Enter item name: ");
			String name = input.nextLine();
			double price = userPricing(input);
			Item i = new Item(name, price);
			store.addItem(i);
			break;
		case "2": // prompt user to create and add produce item
			System.out.print("Enter item name: ");
			String name1 = input.nextLine();
			double price1 = userPricing(input);
			String expiration = userDate(input);
			ProduceItem p = new ProduceItem(name1, price1, expiration);
			store.addItem(p);
			break;
		case "3": // prompt user to create and add age restricted item
			System.out.print("Enter item name: ");
			String name2 = input.nextLine();
			double price2 = userPricing(input);
			int age = userAge(input);
			AgeRestrictedItem a = new AgeRestrictedItem(name2, price2, age);
			store.addItem(a);
			break;
		default: 
			System.out.println("That was not an option.");	
		}
		
		return store;
	}
	
	// The removeItem method allows the user to add items to a cart until they choose to check out. 
	// The customer can also return items back to the store inventory for they check out.
	// The bought items are removed from the store.
	public static Inventory removeItem(Inventory store, Scanner input) {
		Inventory cart = new Inventory();
		boolean done = false;
		do {
			// prompt user for item and search the store
			System.out.print("Enter the item you wish to add to your the cart: ");	
			String choice = input.nextLine();
			store.sortStock();
			int index = store.searchStock(choice, 0, store.size());
			if (index == -1) {
				System.out.println("Item not found.");
			} else {
				System.out.println("Your item costs " + store.getStock(index).getPrice() + " dollars. Do you want to add it to your cart? Enter yes or no.");
				String yn = input.nextLine();
				switch (yn.toLowerCase()) {
				case "yes": // add item to cart
				case "y":
					cart.addItem(store.getStock(index)); 
					System.out.println(store.getStock(index).cartMessage()); 
					store.removeItem(index);
					break;
				case "no": // do not add item to cart
				case "n":
					System.out.println("Item was not added to cart."); 
					break;
				default:
					System.out.println("That was not an option.");
				}
			} 
			System.out.println("Would you like to check out? Enter yes to checkout or anything else to continue shopping.");
				String yn = input.nextLine();
				switch (yn.toLowerCase()) {
				case "yes": // get users choice to check out
				case "y":
					done = true;
					break;
				} 
		} while (!done);
			
		
		boolean done1 = false;
		do {
			System.out.println("Would you like to remove anything from your cart before you complete the checkout? Enter yes or no.");
			String cartDecision = input.nextLine();
			switch (cartDecision) {
			case "yes": // allow user to remove item from cart before check out if they desire
			case "y":
				System.out.println("Enter the name of the item you would like to remove from your cart: ");
				String item = input.nextLine();
				cart.sortStock();
				int index = cart.searchStock(item, 0, cart.size()); // search cart and remove if found
				if (index == -1) {
					System.out.println("That item is not in your cart.");
				} else {
					store.addItem(cart.getStock(index));
					cart.removeItem(index);
					System.out.println(item + " has been removed from the cart.");
				}
				break;
			case "no":
			case "n":
				done1 = true;
				break;
			default:
				System.out.println("That was not an option.");
			}
		} while (!done1);

			System.out.println("Your total is " + cart.calcTotal() + " dollars.");
		
		return store;	
	}
	
	// This method allows to search for an item in the store to modify.
	// The user will be prompted for the information pertaining to the item type.
	// All of that item will be modified.
	public static Inventory modifyStock(Inventory store, Scanner input) {
		// search for item
		System.out.print("Enter the name of the item you would like to modify: ");
		String item = input.nextLine();
		store.sortStock();
		int index = store.searchStock(item, 0, store.size());
		if (index == -1) {
			System.out.println("Item not found.");
		} else {
			// get name and price from user for a regular item
			// loop through and modify every instance of the item selected
			if ((store.getStock(index) instanceof Item) && !(store.getStock(index) instanceof ProduceItem) && !(store.getStock(index) instanceof AgeRestrictedItem)) {
				System.out.println("The selected item is a regular item.");
				System.out.print("Enter the new item name: ");
				String name = input.nextLine();
				double price = userPricing(input);
				for (int i=0; i<store.size(); i++) {
					if ((store.getStock(i).nameEquals(item)) && (store.getStock(i) instanceof Item)) {
						store.getStock(i).setName(name);
						store.getStock(i).setPrice(price);
					}
				}
			} else if (store.getStock(index) instanceof ProduceItem) { // get information for produce item
				System.out.println("The selected item is a produce item.");
				System.out.print("Enter the new item name: ");
				String name = input.nextLine();
				double price = userPricing(input);
				String expiration = userDate(input);
				ProduceItem p = new ProduceItem(name, price, expiration);
				int position = 0;
				int counter = 0;
				do {
					position = store.searchStock(item, 0, store.size()); // search for and remove every instance of the produce item to be modified
					if (position != -1) {
						store.removeItem(position);
						counter++;
					}
				} while (position != -1);
				for (int k=0; k<counter; k++) {
					store.addItem(p); // add the same amount of the new produce item to the store
				}
			} else if (store.getStock(index) instanceof AgeRestrictedItem) { // get information for age restricted item
				System.out.println("The selected item is an age restricted item.");
				System.out.print("Enter the new item name: ");
				String name = input.nextLine();
				double price = userPricing(input);
				int age = userAge(input);
				AgeRestrictedItem a = new AgeRestrictedItem(name, price, age);
				int position = 0;
				int counter = 0;
				do {
					position = store.searchStock(item, 0, store.size()); // search for and remove every instance of the age restricted item to be modified
					if (position != -1) {
						store.removeItem(position);
						counter++;
					}
				} while (position != -1);
				for (int k=0; k<counter; k++) {
					store.addItem(a); // add the same amount of the new age restricted item to the store
				
				}
			}
		}
		store.sortStock(); // sort store after modification
		return store;
	}
	
	// The searchStock allows the user to search the store using the Binary Search Algorithm.
	public static void searchStore(Inventory store, Scanner input) {
		System.out.print("Enter the name of the item you would like to search for: ");
		String word = input.nextLine();
		store.sortStock();
		int index = store.searchStock(word, 0, store.size());
		if (index == -1) {
			System.out.println("Item not found.");
		} else {
			System.out.println("Your item is located at index " + index + ".");
		}
		
	}
	
	// The printStock method prints the inventory to the console.
	public static void printStock(Inventory store) {
		System.out.println(store);
	}

}
